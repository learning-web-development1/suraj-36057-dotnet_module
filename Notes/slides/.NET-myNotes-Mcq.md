1. Q. Which feature enables to obtain information about the use and capabilities of types at runtime?
- a) Runtime type ID
- b) Reflection
- c) Attributes
- d) None of the mentioned
- 
- Answer: b
- Explanation: Reflection is the feature that enables you to obtain information about a type.
 The term reflection comes from the way the process works:
 A Type object mirrors the underlying type that it represents.
 Reflection is a powerful mechanism because it allows us to learn and use the capabilities 
of types that are known only at runtime.

2.  Q.  Which of the following are correct?

    1. Delegates are like C++ function pointers.
  
    2. Delegates allow methods to be passed as parameters.
  
    3. Delegates can be used to define callback methods.
  
    4. Delegates are not type safe.


a.  1,3,4
b.  1,2,3
c.  2,3,4
d.  All of the above

- ANSWER: 1,2,3
  - A delegate is an object that can refer to a method. It means that it can hold a reference to a method.
  - The method can be called through this reference or we can say that a delegate can invoke the method to which it refers.
 The main advantage of a delegate is that it invokes the method at run time rather than compile time.
  -  Delegate in C# is similar to a function pointer in C / C++.
A delegate can call only those methods that have same signature and return type as delegate. So the option 1, 2, 3 are correct.

- day 1 mcq 

```c# 

1. CLR is the .Net equivalent of _____.
Java Virtual machine
Common Language Runtime
Common Type System
Common Language Specification
Ans:  A

2. Abstract class contains _____.
Abstract methods
Non Abstract methods
Both
None
Ans:  C. 

2.1. Arrays in C# are ______ objects.
A. Reference

B. Logical

C. Value

D. Arithmetic

Ans: A

3. What is the need for ‘Conversion of data type’ in C#?
a) To store a value of one data type into a variable of another data type
b) To get desired data
c) To prevent situations of runtime error during change or conversion of data type
d) None of the mentioned

Answer: c

4. Which of the following Session Mode Serialization is not required to store the data?
A: InProc
B: SQLServer
C: StateServer
D: None of the above
Answer A:Inproc

5. default nature of Dll is ? a)inproc b)outproc  c)both  d) none   ANS: A   = default nature of dll is inproc 

6. A variable which is declared inside a method is called a________variable?

A. Serial
B. Local
C. Private
D. Static

Answer:B

7. Why strings are of reference type in C#.NET?
a) To create string on stack
b) To reduce the size of string
c) To overcome problem of stackoverflow
d) None of the mentioned
View Answer

Answer: b
Explanation: The problem of stack overflow very likely to occur since transport protocol used on web these days are ‘HTTP’ and data standard as ‘XML’. Hence, both make use of strings extensively which will result in stack overflow problem. So, to avoid this situation it is good idea to make strings a reference type and hence create it on heap.



8. What will be the error in the following C# code?

 Static Void Main(String[] args)
 {
     const int m = 100;
     int n = 10;
     const int k = n / 5 * 100 * n ;
     Console.WriteLine(m * k);
     Console.ReadLine();
 }

a) ‘k’ should not be declared constant
b) Expression assigned to ‘k’ should be constant in nature
c) Expression (m * k) is invalid
d) ‘m ‘ is declared in invalid format

Answer: b

Explanation:’k’ should be declared as const int k = 10/5 * 100*10 i.e only constant values should be assigned to a constant.

9. Which among the following does not belong to the C#.NET namespace?
a) class
b) struct
c) enum
d) data
answer D

 10. In ASP.NET application DLL files are stored in which folder?


a. App_Code
b. App_Data
c. Bin
d. App_LocalResources

Answer c.

11. Which of the following utilities can be used to compile managed assemblies into processor-specific native code?

A.	gacutil
B.	ngen
C.	sn
D.	dumpbin
E.	ildasm
Answer: Option B

12. Which of the following statements are TRUE about the .NET CLR?

1) It provides a language-neutral development & execution environment.
2)It ensures that an application would not be able to access memory that it is not authorized to access.
3)It provides services to run "managed" applications.
4)The resources are garbage collected.
5)It provides services to run "unmanaged" applications.

A.Only 1 and 2
B.Only 1, 2 and 4
C.1, 2, 3, 4
D.Only 4 and 5
E.Only 3 and 4

Answer: Option C



13. Which of the following is not a namespace in the .NET Framework Class Library?
a) System.Process
b) System.Security
c) System.Threading
d) System.xml

Answer: A

14. Which of the following components of the .NET framework provide an extensible set of classes that can be used by any .NET compliant programming language?

A. NET class libraries
B. Common Language Runtime
C. Common Language Infrastructure
D. Component Object Model
E. Common Type System

ans :- A

15. Which of the following are true about .NET Framework?
A. It provides a consistent object-oriented programming environment whether object code is stored and executed locally, executed locally but Internet-distributed, or executed remotely.
B. It provides a code-execution environment that minimizes software deployment and versioning conflicts.
C. It provides a code-execution environment that promotes safe execution of code, including code created by an unknown or semi-trusted third party.
D. All of the above

Ans : D, all of the above

16. Whats the full form of MSIL?

A. Microsoft Intermediate Language
B. Microsoft Information Language
C. Microsoft Information Lab
D. Master Software Intermediate Language
E. Mobile Software Intermediate Language

Answer: A
When compiling to managed code, the compiler translates your source code into Microsoft intermediate language (MSIL), 
which is a CPU-independent set of instructions that can be efficiently converted to native code. MSIL includes instructions for loading,
storing, initializing, and calling methods on objects, as well as instructions for arithmetic and logical operations, control flow,
direct memory access, exception handling, and other operations.

17. The CLR is physically represented by an assembly named _______
A. mscoree.dll
B. mcoree.dll
C. msoree.dll
D. mscor.dll
Ans: A

18. Which of the following directive is used to link an assembly to a page or user control?
[A] @Page
[B] @Import
[C] @Assembly
[D] @Reference

Answer: Option [C]

19. Struct’s data members are ___ by default.
A-Protected
B-Public
C-Private
D-Default
Ans:  C

20 .Ctor is the shortcut for writing a constructor in VB c# 
A. True
B. False

Ans:- A. True

21. Which of the following utilities can be used to compile managed assemblies into processor-specific native code?

A.gacutil
B.ngen
C.sn
D.dumpbin
E.ildasm


Answer -B

22. Which of the following .NET components can be used to remove unused references from the managed heap?

A   Common Language Infrastructure
B.  CLR
C.  Garbage Collector
D.  Class Loader
E.  CTS

Answer: Option C

23. To use the .NET Framework Data Provider for SQL Server, an application must reference the _____________ namespace.
a) System.Data.Client
b) System.Data.SqlClient
c) System.Data.Sql
d) None of the mentioned
Answer: b
Explanation: System.Data.SqlClient provides access to versions of SQL Server, which encapsulates database-specific protocols

```

  
- day8

```c#
1. Which among the following is the correct way to find out the number of elements currently present in an ArrayListCollection called arr?
a) arr.Capacity
b) arr.Count
c) arr.MaxIndex
d) arr.UpperBound
Answer: b) arr.Count
D3_Abhishek Gahlaut 36174 10/08 4:36 PM
Question- Does C#.NET support partial implementation of interfaces?
a) True
b) False
c) Can’t Say
d) None of the mentioned

Answer: b
Explanation: Interface is a behaviour. It represents a protocol or a contract of sorts. 
               Hence, it is impossible to implement an interface partially.
D3_Shivam_Alwankar 36506 10/08 4:36 PM
1) What is the .NET collection class that allows an element to be accessed using a unique key?
a) HashTable
b) ArrayList
c) SortedList
d) None

Answer- a)HashTable - It is a key - value pair consists of unique key.So we can access element using a unique key in HashTable.
D3_Pallavi Ghule 36166 10/08 4:38 PM
FileStream class is inherited from which claas?

    A)writer
    B)reader
    C)Stream
    D)None

    Ans=C stream is Base calss for Filestream class.
D3_YogitaBorole 35984 10/08 4:39 PM
Properties in .NET can be declare as

1. Static, Protected internal, Virtual
2. Public, internal, Protected internal
3. Only public
4. None


a. 1, 2
b. 3
c. 1, 2, 3
d. 4                                                                                                                Answer: a
D3_Ajim Sutar 36750 10/08 4:39 PM
1. What is meant by the term generics?
a) parameterized types
b) class
c) structure
d) interface

Answer: a

D3_Lokesh Bramhe 36067 10/08 4:39 PM
Which of these methods of class String is used to compare two String objects for their equality?
a) equals()
b) Equals()
c) isequal()
d) Isequal()
View Answer

Answer: a
D3_Vaibhav 36184 10/08 4:40 PM
Which of the following .NET components can be used to remove unused references from the managed heap?

A.	Common Language Infrastructure
B.	CLR
C.	Garbage Collector
D.	Class Loader
E.	CTS
Answer: Option C
D3_Krunal Urade 36068 10/08 4:42 PM
Q: Which of the following is the correct output of the C#.NET code snippet given below?

int[][] a = new int[2][];
    a[0] = new int[4]{6, 1, 4, 3};
    a[1] = new int[3]{9, 2, 7};
    Console.WriteLine(a[1].GetUpperBound(0));


Options:

3
4
7
2
Answer: option 4
D3_Pavan Pawar 36652 10/08 4:43 PM
Which of the following is the root of the .NET type hierarchy?
A. System.Object
B. System.Type
C. System.Base
D. System.Parent
E. System.Root

Ans :- A
D3_Shital_Shirole 36266 10/08 4:45 PM
We use ..... to fully abstract a class from its implementation.
 A.Interfaces
 B.objects
 C.Packages
 D.Function Definitions

Answer: Option A Interfaces
D3_Pranay_Bawankar 36079 10/08 4:47 PM
Q) What are characteristics best define .NET Core?
A. Flexible deploymentB. Cross-platformC. Command-line toolsD. All of the above

ans D
D3_Sana Jenab 36038 10/08 4:49 PM
Abstract class contains _____.

A.Abstract methods
B.Non Abstract methods
C.Both
D.None

Ans:  C
D3_Shivam 36106 10/08 4:49 PM
Q. Choose the file mode method which is used to create a new output file with the condition that the file with same name must not exist.

A. FileMode.CreateNew
B. FileMode.Create
C. FileMode.OpenOrCreate
D. FileMode.Truncate

ANS : A
D3_Shivam 36106 10/08 4:52 PM
Q. Choose the output returned when read() reads the character from the console?

A. String
B. Char
C. Integer
D. Boolean

ANS : C

Explanation:
Read() returns the character read from the console. It returns the result. The character is returned as an int, which should be cast to char.
You 10/08 4:55 PM
Q. Choose the advantages of using generics?
a) Generics facilitate type safety
b) Generics facilitate improved performance and reduced code
c) Generics promote the usage of parameterized types
d) All of the mentioned


Answer: d
Explanation: By definition of generics.
D3_Gauri Kulkarni 36113 10/08 4:56 PM
Choose the file mode method which is used to create a new output file with the condition that the file with same name must not exist.
A. FileMode.CreateNew B. FileMode.Create   C. FileMode.OpenOrCreate            D. FileMode.Truncate

Answer & Explanation
Answer: Option A

Explanation:

Creates a new output file. The file must not already be existing.
D3_Tushar  36066 10/08 5:06 PM
Q.Which Is The Following Is Not A Component Of The CLR?
 A.Class loader
 B.Garbage collector
 C.NET Framework
 D.JIT Compiler

 Ans. Option C
D3_Akash 36020 10/08 5:13 PM
A type of class which does not have its own objects but acts as a base class for its subclass is known as?
a) Static class
b) Sealed class
c) Abstract class
d) None of the mentioned
Ans: C
D3_Tushar Pawar 36717 10/08 5:58 PM
Q) Which of the following statements are correct about JIT?

1)JIT compiler compiles instructions into machine code at run time.
2)The code compiler by the JIT compiler runs under CLR.
3)The instructions compiled by JIT compilers are written in native code.
4)The instructions compiled by JIT compilers are written in Intermediate Language (IL) code.
5)The method is JIT compiled even if it is not called

A.	1, 2, 3
B.	2, 4
C.	3, 4, 5
D.	1, 2

Answer:  A
D3_Akshay 36516 10/08 6:32 PM
Which of the following security features can .NET applications avail?

1)PIN Security
2)Code Access Security
3)Role Based Security
4)Authentication Security
5)Biorhythm Security
A. 1, 4, 5
B. 2, 5
C. 2, 3
D. 3, 4
Answer-C
D3_Kiran 36453 10/08 7:32 PM
Which of the following statements are correct about functions?
a) C# allows a function to have arguments with default values
b) Redefining a method parameter in the method’s body causes an exception
c) C# allows function to have arguments with default values
d) Omitting the return type in method definition results into exception
Ans - A
```

- day9

```c#
Q. Which feature enables to obtain information about the use and capabilities of types at runtime?
a) Runtime type ID
b) Reflection
c) Attributes
d) None of the mentioned


Answer: b
Explanation: Reflection is the feature that enables you to obtain information about a type.
 The term reflection comes from the way the process works:
 A Type object mirrors the underlying type that it represents.
 Reflection is a powerful mechanism because it allows us to learn and use the capabilities 
of types that are known only at runtime.
D3_Shivam_Alwankar 36506 10/09 6:20 PM
Q) SOAP Stands for ?
a) Standard Object Access Protocol
b) Simple Object Access Protocol
c) Synchronized Object Access Protocol
d) System Object Access Protocol

ANSWER-- b) Simple Object Access Protocol
D3_Lokesh Bramhe 36067 10/09 6:21 PM
Which procedure among the following should be used to implement a ‘Has a’ or a ‘Kind of’ relationship between two entities?
a) Polymorphism
b) Inheritance
c) Templates
d) All of the mentioned
View Answer

Answer: b
D3_Tushar Pawar 36717 10/09 6:21 PM
Q) Which of the following statements are correct about a .NET Assembly?

    1)It is the smallest deployable unit.
    2)Each assembly has only one entry point - Main(), WinMain() or DLLMain().
    3)An assembly can be a Shared assembly or a Private assembly.
    4)An assembly can contain only code and data.
    5)An assembly is always in the form of an EXE file.

A.	1, 2, 3
B.	2, 4, 5
C.	1, 3, 5
D.	1, 2

Answer: Option A
D3_Shivam 36106 10/09 6:21 PM
Q. Choose the namespace which consists of classes that are part of .NET Reflection API?
a) System.Text
b) System.Name
c) System.Reflection
d) None of the mentioned

Answer: c
Explanation: Many of the classes that support reflection are part of the .NET Reflection API, which is in the System.Reflection namespace.
eg : using System.Reflection
D3_Tushar  36066 10/09 6:21 PM
Q. Code that targets the Common Language Runtime is known as

A. Unmanaged
B. Distributed
C. Legacy
D. Managed Code
E. Native Code

Answer: Option D
D3_Kiran 36453 10/09 6:24 PM
Q. Choose the class From which the namespace 'System.Type' is derived:

a) Systems.Reflection
b) System.Reflection.MemberInfo
c) Both a & b
d)None of the mentioned
Answer - a
D3_Sana Jenab 36038 10/09 6:24 PM
You can define one namespace inside another namespace.

A - true

B - false

Answer : A
Explanation
You can define one namespace inside another namespace. You can access members of nested namespace by using the dot (.) operator.
D3_YogitaBorole 35984 10/09 6:25 PM
Serialization techniques ?
a) Binary
b) XML
c) SOAP
d) ALL of the above
e) None

Answer= d) all of the above
D3_Gauri Kulkarni 36113 10/09 6:26 PM
Which Is The Following Is Not A Component Of The CLR?
1)Class loader
 2) Garbage collector
3).NET Framework
4)JIT Compiler
Ans. Option C
D3_Shital_Shirole 36266 10/09 6:29 PM
What does the following declaration specify?

 MethodInfo[] GetMethods()
a) Returns an array of MethodInfo objects
b) Returns a list of the public methods supported by the type by using GetMethods()
c) Both Returns an array of MethodInfo objects & Returns a list of the public methods supported by the type by using GetMethods()
d) None of the mentioned

Answer: c
Explanation: A list of the public methods supported by the type can be obtained by using GetMethods(). It returns an array of MethodInfo objects that describe the methods supported by the invoking type. MethodInfo is in the System.Reflection namespace.
D3_Pavan Pawar 36652 10/09 6:31 PM
Which of the following are parts of the .NET Framework?
   1.The Common Language Runtime (CLR)
   2.The Framework Class Libraries (FCL)
   3.Microsoft Published Web Services 
   4.Applications deployed on IIS
   5.Mobile Applications

A. Only 1, 2, 3
B. Only 1, 2
C. Only 1, 2, 4
D. Only 4, 5
E. All of the above

ans :- B
D3_Krunal Urade 36068 10/09 6:33 PM
	
Q: Which of the following can be facilitated by the Inheritance mechanism?

1.Use the existing functionality of base class.
2.Overrride the existing functionality of base class.
3.Implement new functionality in the derived class.
4.Implement polymorphic behaviour.
5.Implement containership.

A.	1, 2, 3
B.	3, 4
C.	2, 4, 5
D.	3, 5
ANSWER : A
D3_Abhishek Dhale 36646 10/09 6:34 PM
Which of the following jobs are done by Common Language Runtime?

1.It provides core services such as memory management, thread management, and remoting.
2.It enforces strict type safety.
3.It provides Code Access Security.
4.It provides Garbage Collection Services.
A.	Only 1 and 2
B.	Only 3, 4
C.	Only 1, 3 and 4
D.	Only 2, 3 and 4
E.	All of the above
Answer: Option E
D3_Amol 36064 10/09 6:36 PM
Q. Which of the following attributes should you add to a class to enable it to be serialized?

A) ISerializable
B) Serializable
C) SoapInclude
D) OnDeserialization

ANSWER: B

Explanation:

If you apply Serializable attribute to a type, it indicate that instances of this type can be serialized. If you do not want a field within your class to be serializable, apply the NonSerializedAttribute attribute.
D3_Akash 36020 10/09 6:39 PM
Which of the following cannot be used to declare an interface correctly?
a) Properties
b) Methods
c) Structures
d) Events
Ans:C
D3_Vaibhav 36184 10/09 6:43 PM
Which of the following is the root of the .NET type hierarchy?
A. System.Object
B. System.Type
C. System.Base
D. System.Parent
E. System.Root

Ans :- A
D3_Pallavi Ghule 36166 10/09 6:47 PM
Arrays in C# are ______ objects.
A. Reference

B. Logical

C. Value

D. Arithmetic

Ans: A
D3_Abhishek Gahlaut 36174 10/09 7:00 PM (Edited)
Question. Select the method used to write single byte to a file?
a) Write()
b) Wrteline()
c) WriteByte()
d) All of the mentioned

Answer: c
Explanation: To write a byte to a file, the WriteByte( ) method is used. Its simplest form is shown here:
  Ex- void WriteByte(byte value)
D3_Akshay 36516 10/09 7:31 PM
Which of the following converts a type to an unsigned int type in C#?

A - ToType
B - ToUInt16
C - ToSingle
D - ToString

Answer-B
D3_sagarmehar 36062 10/09 7:51 PM
Sealed Classes cannot be a base class?
a-Yes
b-No
c-Both
d-None

ans-a

```

- day10

```c#
Which of the following are the correct statements about delegates?

A. Delegates can be used to implement callback notification

B. Delegates permit execution of a method on a secondary thread in an asynchronous manner

C. Delegate is a user defined type

D. All of the mentioned

Answer- D All of the mentioned


D3_Shital_Shirole 36266 10/12 6:09 PM
To generate a simple notification for an object in runtime, the programming construct to be used for implementing this idea?
a) namespace
b) interface
c) delegate
d) attribute
Answer:  Option C
D3_Lokesh Bramhe 36067 10/12 6:09 PM
To implement delegates, the necessary condition is?
a) class declaration
b) inheritance
c) runtime polymorphism
d) exceptions
View Answer

Answer: a
D3_Tushar Pawar 36717 10/12 6:10 PM
Q) Which of the following are valid .NET CLR JIT performance counters?
1) Total memory used for JIT compilation
2) Average memory used for JIT compilation
3) Number of methods that failed to compile with the standard JIT
4) Percentage of processor time spent performing JIT compilation
5) Percentage of memory currently dedicated for JIT compilation
	A) 1, 5
	B) 3, 4
	C) 1, 2
	D) 4, 5

Answer : B
D3_Pavan Pawar 36652 10/12 6:10 PM
Choose the statements which makes delegate in C#.NET different from a normal class?
A. Delegates in C#.NET is a base class for all delegates type
B. Delegates created in C#.NET are further not allowed to derive from the delegate types that are created
C. Only system and compilers can derive explicitly from the Delegate or Multi caste Delegate class
D. All of the mentioned

ans :- D
D3_Pallavi Ghule 36166 10/12 6:11 PM
Q. Choose the class From which the namespace 'System.Type' is derived:

a) Systems.Reflection
b) System.Reflection.MemberInfo
c) Both a & b
d)None of the mentioned
Answer - a
D3_Akash 36020 10/12 6:11 PM
Choose the incorrect statement among the following about the delegate?
a) delegates are of reference types
b) delegates are object oriented
c) delegates are type safe
d) none of the mentioned
Answer::D
D3_Shivam 36106 10/12 6:15 PM
Q. What is POCO?

A) Plain Old C Object
B) Platform Oriented CLR Object
C) Plain Old CLR Object
D) None

Ans: C)
D3_Tushar  36066 10/12 6:15 PM
Q. Which of the following components of the .NET framework provide an extensible set of classes that can be used by any .NET compliant programming language?


A  .NET class libraries

B  Common Language Runtime

C  Common Language Infrastructure

D  Component Object Model

E  Common Type System

Answer: Option A
D3_Shivam_Alwankar 36506 10/12 6:20 PM
Q. Choose the method defined by MemberInfo.
a) GetCustomAttributes()
b) IsDefined()
c) GetCustomeAttributesData()
d) All of the mentioned

** Answer: d
** Explanation:  MemberInfo includes two abstract methods: GetCustomAttributes( ) and IsDefined( ). These both relate to attributes. The first obtains a list of the custom attributes associated with the invoking object. The second determines if an attribute is defined for the invoking object. The .NET Framework Version 4.0 adds a method called GetCustomAttributesData(), which returns information about custom attributes.
D3_Sana Jenab 36038 10/12 6:23 PM
Which of the following keywords is used to refer base class constructor to subclass constructor?
A. this
B. static
C. base
D. extend
Ans: C
D3_Gauri Kulkarni 36113 10/12 6:25 PM
Q.1 According to given below statements, choose the correct option regarding serialization.

Statement 1: Serialization is the process of converting information into a byte stream that can be stored or transferred.

Statement 2: Deserialization is the process of converting a serialized sequence of bytes into an object.

Statement 3: Serialization is the process of converting a serialized sequence of bytes into an object.

Statement 4: DeSerialization is the process of converting information into a byte stream that can be stored or transferred.

A) Statement 1 and 2 are correct.
B) Statement 3 and 4 are correct.
C) Statement 1 and 4 are correct.
D) Statement 2 and 3 are correct.

ANSWER: A
D3_Abhishek Gahlaut 36174 10/12 6:34 PM
Question. Choose the class from which the namespace ‘System.Type’ is derived?
a) System.Reflection
b) System.Reflection.MemberInfo
c) Both System.Reflection & System.Reflection.MemberInfo
d) None of the mentioned

Answer: b
Explanation: Type is derived from an abstract class called System.Reflection.MemberInfo
D3_Ajim Sutar 36750 10/12 6:43 PM
To implement delegates, the necessary condition is?
a) class declaration
b) inheritance
c) runtime polymorphism
d) exceptions
Answer: a
D3_Amol 36064 10/12 6:46 PM
Q. The [Serializable()] attributes gets inspected at _________
a) compile time
b) run time
c) design time
d) linking time
Ans: b
D3_Vaibhav 36184 10/12 6:53 PM
2. To implement delegates, the necessary condition is?
a) class declaration
b) inheritance
c) runtime polymorphism
d) exceptions                    Answer:- a
D3_Krunal Urade 36068 10/12 7:21 PM
Q: Which among the following differentiates a delegate in C#.NET from a conventional function pointer in other languages?
a) delegates in C#.NET represent a new type in the Common Type System
b) delegates allows static as well as instance methods to be invoked
c) delegates are type safe and secure
d) none of the mentioned
View Answer

Answer: d
You 10/12 7:53 PM
Q.  Which of the following are correct?

1. Delegates are like C++ function pointers.
2. Delegates allow methods to be passed as parameters.
3. Delegates can be used to define callback methods.
4. Delegates are not type safe.


a. 1,3,4
b. 1,2,3
c. 2,3,4
d. All of the above

ANSWER: 1,2,3
A delegate is an object that can refer to a method. It means that it can hold a reference to a method.
The method can be called through this reference or we can say that a delegate can invoke the method to which it refers.
 The main advantage of a delegate is that it invokes the method at run time rather than compile time.
 Delegate in C# is similar to a function pointer in C / C++.
A delegate can call only those methods that have same signature and return type as delegate. So the option 1, 2, 3 are correct.

```

- day11

```c#
Q.  Which interface defines the basic extension methods for LINQ?

A). IComparable<T>
B). IList
C). IEnumerable
D). IQueryable<T>

ANSWER: C) IEnumerable
 
Explanation : IEnumerable is a generic interface that defines the basic extension methods for LINQ. IEnumerable can move forward only over a collection. It is suitable for LINQ to Object and LINQ to XML queries.
D3_Shital_Shirole 36266 Yesterday 6:09 PM
Select the namespace which should be included while making use of LINQ operations?
a) System.Text
b) System.Collections.Generic
c) System.Linq
d) None of the mentioned
Answer: c
Explanation: By definition.
D3_Tushar  36066 Yesterday 6:09 PM
Q. Which of the following statements is correct about a delegate?
a) inheritance is a prerequisite for using delegates
b) delegates are not type safe
c) delegates provides wrappers for function pointers
d) none of the mentioned

Answer: c
D3_YogitaBorole 35984 Yesterday 6:10 PM
When do LINQ queries actually run?
a. When they are iterated over in a foreachloop
b. When calling the ToArray() method on the range variable
c. When calling the ToList() method on the range variable
d. All of the above
ANSWER: All of the above
Explanation:
In the above question all options are correct. By default LINQ uses deferred query execution. It means that LINQ queries are always executed when you iterated the query variable, not when the query variable is created. LINQ queries actually run when they are iterated over in a foreach loop.
D3_Pallavi Ghule 36166 Yesterday 6:11 PM
2.Which of the following are the correct statements about delegates?
    A. Delegates can be used to implement callback notification
    B. Delegates permit execution of a method on a secondary thread in an asynchronous manner
    C. Delegate is a user defined type
    D. All of the mentioned

    Ans:D (All are true about delegate)
D3_Akash 36020 Yesterday 6:12 PM
What types of Objects can you query using LINQ?
a. DataTables and DataSets
b. Any .NET Framework collection that implements IEnumerable(T)
c. Collections that implement interfaces that inherit from IEnumerable(T)
d. All of the above

ANSWER: All of the above

Explanation:
 Any .NET Framework collection that implements IEnumerable(T) or that inherit from IEnumerable(T) can be queried using LINQ.
D3_Ajim Sutar 36750 Yesterday 6:13 PM
What is lamda expression?

1. Anonymous function
2. Can  be used to create delegates
3. Named function
4. None

Answer:1,2

A lambda expression is an anonymous function that can be used to create delegates.
D3_Lokesh Bramhe 36067 Yesterday 6:18 PM
Which of the following statement is correct?

a. Anonymous types are class types that derive directly from object.
b. Anonymous types are not class types that derive directly from object.

 
c. Anonymous types are class types that derive directly from System.Class.
d. None of the above
Answer  Explanation 
ANSWER: Anonymous types are class types that derive directly from object.

Explanation:
Anonymous types are class types that derive directly from object. You cannot cast to other type except object.Anonymous types are created by using new operator along with an object initializer.

Example:
var v = new {Website = “ CareerRide ”, Message = " Welcome " };
Console.WriteLine(v. Website + v.Message);
D3_Tushar Pawar 36717 Yesterday 6:18 PM
Q) Which of the following assemblies can be stored in Global Assembly Cache?

	A) Private Assemblies
	B) Friend Assemblies
	C) Shared Assemblies
	D) Public Assemblies
	E) Protected Assemblies
Answer: Option C
D3_Shivam_Alwankar 36506 Yesterday 6:20 PM
Q) Expression tree is _________.

A Tree of expression in C# or VB.

B) Executable tree structure.

C) In-memory representation of lambda expression.

D)Binary tree like data structure for better LINQ support.

Answer: c-  In-memory representation of lambda expression.
You Yesterday 6:37 PM
LINQ supports which of the following syntaxes?

A. Query syntax

B. Method syntax

C. All of the above

D. None of the above

Ans C
D3_Abhishek Dhale 36646 Yesterday 6:45 PM
When do LINQ queries actually run?

a. When they are iterated over in a foreachloop
b. When calling the ToArray() method on the range variable
c. When calling the ToList() method on the range variable
d. All of the above
Answer  Explanation 
ANSWER: All of the above

Explanation:
In the above question all options are correct. By default LINQ uses deferred query execution. It means that LINQ queries are always executed when you iterated the query variable, not when the query variable is created. LINQ queries actually run when they are iterated over in a foreach loop.
D3_Pavan Pawar 36652 Yesterday 6:53 PM
Which of the following statements is true?

a. LINQ to SQL works with any database.
b. LINQ to SQL works with SQL Server.
c. LINQ to SQL is a CLR feature.
d. LINQ to SQL is a SQL Server feature.

Ans:- B
D3_Sana Jenab 36038 Yesterday 7:20 PM
The full form of LINQ is _______.

A. Link-List Inner Query
B. Language-Integrated Query
C. Linked-Integrated Query
D. Lazy Integration Query


Answer : Option B
D3_Kiran 36453 Yesterday 7:25 PM
Q. Choose the correct one.
a. The return value of the lambda (if any) must be explicitly convertible to the delegate's return type
b. Each input parameter in the lambda must be implicitly convertible to its corresponding delegate parameter.
c. Lamda expression does not work with LINQ.
d. None of the above
Answer : B
Explanation:
Each input parameter in the lambda must be implicitly convertible to its corresponding delegate parameter because A delegate can refer only those methods that have same signature as delegate.
D3_Krunal Urade 36068 Yesterday 7:35 PM
Q: How you can merge the results from two separate LINQ queries into a single result set.


a. Use the ToList method.
b. Use the DataContractJsonSerializer class.
c. Use the XElement class.
d. Use the Concat method.


ANSWER: Use the Concat method.
D3_Gauri Kulkarni 36113 Yesterday 7:35 PM
When do LINQ queries actually run?

a. When they are iterated over in a foreachloop
b. When calling the ToArray() method on the range variable
c. When calling the ToList() method on the range variable
d. All of the above
ANSWER: All of the above

Explanation:
In the above question all options are correct. By default LINQ uses deferred query execution. It means that LINQ queries are always executed when you iterated the query variable, not when the query variable is created. LINQ queries actually run when they are iterated over in a foreach loop.
D3_Amol 36064 Yesterday 7:48 PM
Q. delegate is a _______
a)reference type
b)value type
c)both
d)none of above
Answer : a
D3_Vaibhav 36184 Yesterday 8:22 PM
Question. Choose the class from which the namespace ‘System.Type’ is derived?
a) System.Reflection
b) System.Reflection.MemberInfo
c) Both System.Reflection & System.Reflection.MemberInfo
d) None of the mentioned

Answer: B
Explanation: Type is derived from an abstract class called System.Reflection.MemberInfo
D3_sagarmehar 36062 Yesterday 8:54 PM
What types of Objects can you query using LINQ?
a. DataTables and DataSets
b. Any .NET Framework collection that implements IEnumerable(T)
c. Collections that implement interfaces that inherit from IEnumerable(T)
d. All of the above
ans-d

Explanation- Any .NET Framework collection that implements IEnumerable(T) or 
that inherit from IEnumerable(T) can be queried using LINQ.
D3_Akshay 36516 Yesterday 9:49 PM
Which query expression is used to limit the number of results?

a. Skip
b. Take
c. Where
d. Select

Answer-B

Explanation:
Take function takes a number of elements from one collection, and places them in a new collection.

```

- day12

```c#

```



## Notes 

1.