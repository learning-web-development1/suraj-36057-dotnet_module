﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3_report_factory
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("please select extension of report \n 1.PDF \n 2.Excel \n 3.Word \n 4.Txt");

            int choice = Convert.ToInt32(Console.ReadLine());

            ReportFactory reportfactory = new ReportFactory();
            Report report = reportfactory.GenerateReport(choice);
            report.Generate();
            

            Console.ReadLine();

        }
    }

    public class ReportFactory
    {
        public Report GenerateReport(int choice)
        {
            if(choice == 1)
            {
                return new PdfReport();
            }
            else if(choice == 2)
            {
                return new ExcelReport();
            }
            else if (choice == 3)
            {
                return new WordReport();
            } else
            {
                return new TxtReport() ;
            }
        }
    }

    abstract public class Report
    {

       abstract public  void Parse();

       abstract public void Validation();

       abstract public void Save();

        public void Generate()
        {
            Parse();
            Validation();
            Save();
        }
    }
    public class PdfReport:Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parsing done for PDF");
        }
        public override void Validation()
        {
            Console.WriteLine("Validation done for PDF");
        }
        public override void Save()
        {
            Console.WriteLine("Save done for PDF");
        }


    }

    public class ExcelReport:Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parsing done for Excel");
        }
        public override void Validation()
        {
            Console.WriteLine("Validation done for Excel");
        }
        public override void Save()
        {
            Console.WriteLine("Save done for Excel");
        }


    }

    public class WordReport:Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parsing done for Word");
        }
        public override void Validation()
        {
            Console.WriteLine("Validation done for Word");
        }
        public override void Save()
        {
            Console.WriteLine("Save done for Word");
        }


    }

    public class TxtReport : Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parsing done for Txt");
        }
        public override void Validation()
        {
            Console.WriteLine("Validation done for Txt");
        }
        public override void Save()
        {
            Console.WriteLine("Save done for Txt");
        }


    }
}
