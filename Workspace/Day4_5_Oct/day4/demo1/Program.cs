﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Second;
using Second.Third;
namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {

            Cacli c = new Cacli();

            Console.WriteLine( c.Addition(10,20) );
            Console.WriteLine(c.Substraction(20,10));

            Second.Third.Cacli2 c2 = new Cacli2();

            Console.WriteLine(c2.Multiplication(10,10));
            Console.WriteLine(c2.Division(1,100));

            Console.ReadLine();
        }
    }
}

namespace Second
{
    public class Cacli
    {
        public int Addition(int x,int y)
        {
            return x + y;
        }
        public int Substraction(int x, int y)
        {
            return x - y;
        }
    }

    namespace Third
    {
        public class Cacli2
        {
            public int Multiplication(int x, int y)
            {
                return x * y;
            }
            public int Division(int x, int y)
            {
                return x / y;
            }
        }

    }
}
