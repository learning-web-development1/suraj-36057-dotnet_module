﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {

            DemoDBEntities DbObject = new DemoDBEntities();

            var allEMP = DbObject.Emps.ToList();

            #region Select by Entity framework
            /*foreach (var emp in allEMP)
            {
                Console.WriteLine(string.Format(
                    "Emp name = {0} and Emp Address = {1}",
                    emp.Name, emp.Address));

            }*/
            #endregion

            #region Insert by Entity Framework
            /* DbObject.Emps.Add(new Emp()
            {
                No = 41,
                Name = "Divine",
                Address = "mumbai"
            });*/
            #endregion

            #region Update by Entity Framework 
            /*
                        var empTOModify = (from emp in DbObject.Emps.ToList()
                                           where emp.No == 41
                                           select emp).First();

                        empTOModify.Name = "Saurav";
                        empTOModify.Address = "Bihar";

                        DbObject.SaveChanges();
                       */
            #endregion

            #region Delete by Entity Framework
            /*  var empTODelete = (from emp in DbObject.Emps.ToList()
                               where emp.No == 41
                               select emp).First();

            DbObject.Emps.Remove(empTODelete);

            DbObject.SaveChanges();*/
            #endregion

            #region call Stored Procedure by Entity Framework
            // DbObject.spInsert(41, "divine", "mumbai");
            #endregion

            #region adding customer table to entity model, updating in edmx file ,and printing object
            /*  var allCustomer = DbObject.Customers.ToList();

            foreach (var customer in allCustomer)
            {
                Console.WriteLine(string.Format("customer name = {0}  ",customer.CName));

            }
*/
            #endregion



            Console.ReadLine();
        }

    }
}
