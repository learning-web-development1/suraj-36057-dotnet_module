﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3_templates
{
    class Program
    {
        static void Main(string[] args)
        {

            #region swap of integer and double 

            //Maths<T> obj = new Maths<T>();

             /* Maths obj = new Maths();

                int p = 100;
                int q = 200;
               double p1 = 100.5;
               double q1 = 200.5;
               Console.WriteLine(" before swap p = " + p.ToString() + " q = " + q.ToString());

               obj.Swap<double>(ref p1, ref q1);
               Console.WriteLine(" after swap p = " + p1.ToString() + "  q = " + q1.ToString());
   
*/
            /*  Maths obj2 = new Maths();

              int p1 = 100;
              int q1 = 200;

              Console.WriteLine(" before swap p = " + p1.ToString() + " q = " + q1.ToString());

              obj2.Swap<int>(ref p1, ref q1);
              Console.WriteLine(" after swap p = " + p1.ToString() + "  q = " + q1.ToString());

             


  */
            #endregion



            #region Normal Class with normal method
            /*
                        Maths obj3 = new Maths();
                        int result = obj3.Add(10, 20);
                        Console.WriteLine(result); ;*/
            #endregion

            #region Using Normal Class(sm) to inherit generic class Maths
            /*  SpecialMaths obj4 = new SpecialMaths();

              double p1 = 100.5;
              double q1 = 200.5;
              obj4.Swap(ref p1, ref q1);
              Console.WriteLine(" after swap p = " + p1.ToString() + "  q = " + q1.ToString());
  */
            #endregion

            #region  implementing generic class (Special Maths) inheriting from another generic class (Maths)

             SpecialMaths<int, double, short, string> obj5 =
                 new SpecialMaths<int, double, short, string>();
             
            string data = obj5.DoSomething(100, 20.2, 2, "abcd");
            Console.WriteLine(data);

            #endregion

           



            Console.ReadLine();
        }
    }


    #region Generic Class(Maths) with Generic Swap and normal Add
    public class Maths<T>
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
        public void Swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }

        public void Swap(ref double x, ref double y)
        {
            double z;
            z = x;
            x = y;
            y = z;
        }
    }
    #endregion

    #region Normal Class(Maths) with Generic Swap and normal method Add
    /*public class Maths
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
        public void Swap<T>(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }

        *//* public void Swap(ref double x, ref double y)
         {
             double z;
             z = x;
             x = y;
             y = z;
         }*//*
    }*/
    #endregion


    #region Normal Class Inherit Generic class Maths

    /* public class SpecialMaths : Maths<double>
     {

     }*/

    #endregion

    #region generic class (Special Maths) inheriting from another generic class (Maths)

    public class SpecialMaths<P, Q, R, S> : Maths<R>
    {
        public S DoSomething (P p, Q q, R r, S s)
            {
            return s;
            }

    }

    #endregion


}
