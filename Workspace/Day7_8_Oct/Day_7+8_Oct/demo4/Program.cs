﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo4
{
    class Program
    {


static void Main(string[] args)
{



            #region File Writing
            // FileStream fs = new FileStream("E:\\DAC\\claswork-bofore-git-commit\\Day7\\Data.txt")
            // declare stream with path,mode,access

           /* FileStream fs = new FileStream(@"E:\DAC\.NET_module\Classwork\Day7_8_Oct\DataTest.txt",
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);


            StreamWriter writer = new StreamWriter(fs);

            writer.WriteLine("hello world");

            fs.Flush();

            writer.Close();

            fs.Close();*/
            #endregion

            #region read file 
           /* FileStream fs1 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day7_8_Oct\DataTest.txt",
                                           FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs1);

            string data = reader.ReadToEnd();
            *//*while (true)
             {
                string entireData = reader.ReadLine();
                if (entireData != null)
                {
                    Console.WriteLine(entireData);
                }
                else
                {
                    break;
                }
            }
            *//*
            Console.WriteLine(data);
            reader.Close();

            fs1.Close();
*/
            #endregion



            #region Object writing to File 
            // only print class name, cant write class object to file as bit ,also known as serialization 
           /* Employee emp = new Employee();

            Console.WriteLine("Enter No:");
            emp.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            emp.Name = Console.ReadLine();


            FileStream fs3 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day7_8_Oct\DataTest.txt",
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);

            StreamWriter writer = new StreamWriter(fs3);

            writer.WriteLine(emp);

            fs3.Flush();
            writer.Close();
            fs3.Close();*/
            #endregion




            Console.ReadLine();
}
}

public class Employee
{
private int _No;

private string _Name;


public string Name
{
get { return _Name; }
set { _Name = value; }
}

public int No
{
get { return _No; }
set { _No = value; }
}

public string emp_detials()
{
return "employee no = " + this.No.ToString() + " " + "employee name = " + this.Name;
}


}

}
