﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Maths obj = new Maths();
            /* A obj2 = new Maths();

             int result = obj2.Add(10, 20);
             Console.WriteLine(result);
           */

            A obj = new Maths();
            B obj2 = new Maths();
            int result = obj.Add(10, 20);
            Console.WriteLine(result);

            int result2 = obj2.Add(10, 20);
            Console.WriteLine(result2);

            Console.ReadLine();


        }
    }

    public interface A
    {
        int Add(int x, int y);
        int Sub(int x, int y);
    }

    public interface B
    {
        int Add(int x, int y);
        int Mult(int x, int y);
    }

    public class SomeBase
    {

    }

    public class Maths : SomeBase, A, B
    {
        int A.Add(int x, int y)
        {
            return x + y;
        }

        int B.Add(int x, int y)
        {
            return x + y + 100;
        }

        int B.Mult(int x, int y)
        {
            return x * y;
        }

        int A.Sub(int x, int y)
        {
            return x - y;
        }

        public int div(int x, int y)
        {
            return x / y;
        }
    }



    public interface Database
    {
        void Insert();

        void Updated();

        void Deleted();


    }




    public class SQLServer : Database
    {
        public void Insert()
        {
            Console.WriteLine("Sql Server data inserted");
        }
        public void Updated()
        {
            Console.WriteLine("Sql Server data Updated");
        }
        public void Deleted()
        {
            Console.WriteLine("Sql Server data Deleted");
        }
    }

    public class OracleServer : Database
    {
        public void Insert()
        {
            Console.WriteLine("Oracle Server data inserted");
        }
        public void Updated()
        {
            Console.WriteLine("Oracle Server data Updated");
        }
        public void Deleted()
        {
            Console.WriteLine("Oracle Server data Deleted");
        }
    }
}


