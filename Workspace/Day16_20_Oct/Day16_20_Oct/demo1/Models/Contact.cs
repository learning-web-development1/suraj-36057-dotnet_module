﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo1.Models
{
    public class Contact
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string  Query { get; set; }

        public override string ToString()
        {
            return string.Format(" Query info : Name = {0}, Phone = {1}, Email = {2}, Query = {3}",
                this.Name, this.Phone, this.Email, this.Query);


        }
    }
}