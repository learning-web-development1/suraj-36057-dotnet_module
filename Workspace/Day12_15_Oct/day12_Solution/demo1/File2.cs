﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{//developer 2 
    public partial class Emp
    {
        partial void Validate(string propertyName, object Value)
        {
            if(propertyName == "Age")
            {
                int age = Convert.ToInt32(Value);
                if(age < 18 || age > 60 )
                {
                    Console.WriteLine("Age is set up wrong !");
                }
            }

        }
        public string GetDetails()
        {
           return string.Format("employee age  = {0}", this.Age);
        }
    }


    public class Book
    {
       public string GetBookDetails()
        {
            return "Some book Details...";
        }
    }

    public class Factory
    {
        public object GetSomeTypeObject(int choice)
        {
            if(choice == 1)
            {
                return new Emp();
            }
            else
            {
                return new Book();
            }

        }
    }
}

