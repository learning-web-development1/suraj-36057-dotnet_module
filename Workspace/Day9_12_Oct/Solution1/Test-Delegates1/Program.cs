﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Delegates
{
    delegate int mydelegate(int p, int q);
    delegate void stringd(string n);

    class Program
    {
        static void Main(string[] args)
        {
            mydelegate pointer = new mydelegate(Add);

            int result = pointer(10, 20);

            Console.WriteLine("Result = " + result);

            stringd pointer2 = new stringd(SayHi);
            pointer2("hello  ");
            Console.ReadLine();
        }

        public static int Add(int x, int y)
        {
            return x + y;
        }

        public static int Sub(int x, int y)
        {
            return x - y;
        }

        public static void SayHi(string name)
        {
            Console.WriteLine("Hi" + name);
        }
    }
}
