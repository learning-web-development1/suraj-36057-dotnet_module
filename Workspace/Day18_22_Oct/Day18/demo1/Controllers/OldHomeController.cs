﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Mvc;
using demo1.Models;

/*namespace demo1.Controllers
{
    
    public class HomeController : BaseController
    {
        // GET: Home

        //DemoDBEntities dbObj = new DemoDBEntities();
        public ActionResult Index()
        {

            try
            {
                var emp = dbObj.Emps.ToList();

                ViewBag.MyMessage = "Employee list";

                ViewBag.UserName = User.Identity.Name; 
                return View(emp);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }

         
        }
        [AllowAnonymous]
        public ActionResult About()
        {
            try
            {
                ViewBag.MyMessage = "About Us";
                ViewBag.UserName = User.Identity.Name;
                return View();

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }


         
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
            try
            {
                ViewBag.action = "/Home/Contact";
                ViewBag.method = "POST";
                ViewBag.UserName = User.Identity.Name;
                ViewBag.MyMessage = "Contact Us";

                ViewBag.success = "your query has been noted";
                return View();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }
          
        }
        [HttpPost]
        public ActionResult Contact(Contact contact)
        {

            try
            {
                ViewBag.done = contact.ToString();


                string email = ConfigurationManager.AppSettings["Email"];
                string password = ConfigurationManager.AppSettings["Password"];


                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(email);

                mail.To.Add(contact.Email);

                mail.CC.Add("surajporje69@gmail.com");

                mail.Subject = "New Query Recieved";

                mail.Body = "<h2>" + contact.ToString() + "</h2>";

                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential(email, password);

                smtp.EnableSsl = true;

                smtp.Send(mail);

                ViewBag.done = "Your Query submitted Successfully";

                return View();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }

           
        }




        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var empToModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                dbObj.SaveChanges();

                ViewBag.MyMessage = string.Format(" Update Employee {0} Details", empToModify.Name);
                return View(empToModify);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }
            
        }
        [HttpPost]
        public ActionResult Edit(Emp employee)
        {
            try
            {
                var empToModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == employee.No
                                   select emp).First();

                empToModify.Name = employee.Name;

                empToModify.Address = employee.Address;

                dbObj.SaveChanges();



                return Redirect("/Home/Index");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }


           
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                ViewBag.MyMessage = string.Format(" Register New Employee");
                return View();

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }

           
        }

        [HttpPost]
        public ActionResult Create(Emp employee)
        {
            try
            {
                dbObj.Emps.Add(new Emp()
                {
                    No = employee.No,
                    Name = employee.Name,
                    Address = employee.Address


                });



                dbObj.SaveChanges();



                return Redirect("/Home/Create");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }

           
        }


         public ActionResult Delete(int id)
        {
            try
            {
                var empToDelete = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                dbObj.Emps.Remove(empToDelete);


                dbObj.SaveChanges();


                return Redirect("/Home/Index");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);
                return null;
            }
          
        }


    }
}*/