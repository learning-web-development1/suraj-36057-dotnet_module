﻿using demo1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using demo1.Filters;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace demo1.Controllers
{
    [HandleError(ExceptionType = typeof(Exception),View = "Error")]
    
    [CustomFilter]
    [Authorize]
    public class BaseController : Controller
    {
       protected DemoDBEntities dbObj { get; set; }

        public BaseController()
        {
            this.dbObj = new DemoDBEntities();
        }

    }
}