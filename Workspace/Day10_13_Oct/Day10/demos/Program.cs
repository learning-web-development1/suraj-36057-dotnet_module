﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using library;
namespace demos
{
    class Program
    {
        delegate string mydelegate(string name); // in namespace 
        static void Main(string[] args)
        {

            #region nullable
              int? Salary = 0;
              Salary = null;

              int? Salary1 = 110;
              //both are same 
              // Nullable<int> Salary1 = null;

              if (Salary1.HasValue)
              {
                  Console.WriteLine("Salary holds a value");
              }
              else
              {
                  Console.WriteLine("Salary is null");
              }

            #endregion

            #region example
            /*
                       Emp e = new Emp();
                       e.Name = "string";
                       e.can = true;
                       if(e.can)
                       {

                       }*/
            #endregion


            #region Partial class usage
              Maths obj = new Maths();

              obj.Add(10, 20);
              obj.Sub(20, 10);

            #endregion


            #region Anonymous Method

            // calll normal method 
           string message = SayHello("suraj");
            Console.WriteLine(message);

            // SayHello("suraj");
            // call method using  pointer
             mydelegate pointer1 = new mydelegate(SayHello);
             string message2 = pointer1("suraj porje");
              Console.WriteLine(message2);

            //anonymous method
            mydelegate pointer2 = delegate (string name)
            {
                return "Hello" + name;
            };


            string message3 = pointer2("suraj porje");
            Console.WriteLine(message3);


            #endregion

            #region Lambada expression
          /*
          mydelegate pointer1 = new mydelegate(SayHello);
          string message2 = pointer1("suraj porje");
          Console.WriteLine(message2);
          */

            //Lambda expression Syntax
            // same as above code in comment
             mydelegate pointer3 = (name) => {
                 return "Hello" + name;
             };


             string message4 = pointer3("suraj porje");
             Console.WriteLine(message4);
             
            #endregion

            #region Iterator concept implementation
            Week week = new Week();

            foreach (string day in week)
            {
                Console.WriteLine(day);
            }
            #endregion


            #region Implicit Type
            //normal declaration
            int i = 100;
            object obj1 = i;
            Emp e1 = new Emp();

            obj1 = e1;
            //implicit declaration
            var v = 100;
            // v = "a"; // gives error

            var v2 = new Emp();

            // var v3; // gives error implicit must be initialized


            int choice = Convert.ToInt32(Console.ReadLine());
            var v5 = GetSomething(choice);
            //var v6 = null; //cannot assign null to implicit type */

            #endregion

            #region Auto Property
             Emp emp = new Emp();

             emp.No = 1;
             emp.Name = "suraj";

             Console.WriteLine(emp.Name);
            #endregion

            #region Object Initializer 
            //shorthand for assigning values
             var emp2 = new Emp() { No = 100, Name = "suraj", Address = "nasik" };
             var emp3 = new Emp()
             {
                 No = 100,
                 Name = "suraj",
                 Address = "nasik"
             };

            var emp4 = new Emp() { No = 1, Name = "Ram", Address = "nashik" };

             Console.WriteLine(emp2.Name);
            #endregion


            #region Anonymous Type 
            // commpiler creates a class, which we cannot use other place
            // we can see in MSIL 
            // getter are defined, setter are not,it is not allowed -- i.e it is read only 
            // so datatype of object of anonymous class must be var, as we have no knowledge about type of data member
            // so var at compile time get datatype of object returned, so we need to use var, as we dont know what it on RHS
            var emp7 = new { No = 100, Name = "suraj", Address = "nasik" };
            // object emp = new { No = 100, Name = "suraj", Address = "nasik" };
            var emp5 = new { No = 100, Name = "suraj", Address = 1 };

            // both class object have one generic class ,with generic data member

            //now it creates a new class in MSIL for this 
            // as parameter sequence matter, values doesnt
            var emp6 = new { No = 100, Address = 1, Name = "suraj" };
            Console.WriteLine(emp.No);
            Console.WriteLine(emp.Name);
            Console.WriteLine(emp.Address);
            #endregion

            Console.ReadLine();
        }

        //  var v4 = 100; //var is always a local variable 

        public static object GetSomething(int i)
        {
            if (i == 1)
            {
                return 100;
            }
            else
            {
                //return new Emp();
                return 200;
            }
        }


        public static string SayHello(string name)
        {
            return "Hello" + name;
        }

        public static void Do1<P, Q, R, S>(P p, Q q, R r, S s)
        {

        }
        public static void Do2<P, Q, S, R>(P p, Q q, S s, R r)
        {

        }
    }







    public class Week:IEnumerable
    {
        private string[] days =
            new string[] { "mon", "tue", "wed", "thur", "fri", "sat", "sun" };

        public  IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[i];


            }


            // throw new NotImplementedException();
        }

    }


     public class Emp
     {// Auto Property 
         // use prop
         public int No { get; set; }
         public string Name { get; set; }

         public string Address { get; set; }

     }
 

    /* public class Emp
     {
         private string _Name;

         private bool _can;

         private int value;

         public int _value
         {
             get { return value; }
             set { value = value; }
         }


         public bool can
         {
             get { return _can; }
             set { _can = value; }
         }

         public string Name
         {
             get { return _Name; }
             set { _Name = value; }
         }

     }
 */


  
}
