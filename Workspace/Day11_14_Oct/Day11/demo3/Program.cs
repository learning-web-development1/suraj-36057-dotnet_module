﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Emp> emps = new List<Emp>()
          {
                new Emp{ No = 11, Name = "Mahesh"  , Address="pune" },
                new Emp{ No = 12, Name = "Rahul"  , Address="panji" },
                new Emp{ No = 13, Name = "Rajiv"  , Address="mumbai" },
                new Emp{ No = 14, Name = "Ketan"  , Address="manglore" },
                new Emp{ No = 15, Name = "Yogesh"  , Address="banglore" },
                new Emp{ No = 16, Name = "Amit"  , Address="chennai" },
                new Emp{ No = 17, Name = "Nilesh"  , Address="pune" },
                new Emp{ No = 18, Name = "Nitin"  , Address="satara" },
                new Emp{ No = 19, Name = "Vishal"  , Address="kolhapur" },
                new Emp{ No = 20, Name = "Sachin"  , Address="chennai" }
          };

            Console.WriteLine("Enter city as Search Character");

            string cityFilter = Console.ReadLine();




            #region using foreach
            /*
             var result = new List<Emp>();
            foreach (Emp emp in emps)
             {
                 if (emp.Address.StartsWith(cityFilter))
                 {
                     //  Console.WriteLine("Name = " + emp.Name + " City he lives in  " + emp.Address);

                     result.Add(emp);
                 }
              Console.WriteLine("Filtered result is ----------");

            foreach (var res in result)
            {
                Console.WriteLine("Name = " + res.Name + "  lives in  " + res.Address);

            }


             }*/
            #endregion

            #region using linq lazy loading ,i.e execute query when first LHS(result) used
            /* var result = from emp in emps
                          where emp.Address.StartsWith(cityFilter)
                          select emp;

             emps.Add(new Emp() { No = 21, Name = "suraj", Address = "nasik" });

             Console.WriteLine("Filtered result is ----------");

             foreach (var res in result)
             {
                 Console.WriteLine("Name = " + res.Name + "  lives in  " + res.Address);

             }*/
            #endregion

            #region linq with use of anonymous class + ToList

            #endregion
            var result = (from emp in emps
                          where emp.Address.StartsWith(cityFilter)
                          select new 
                          {
                              EName = "Mr/Mrs " + emp.Name,
                              ECity = emp.Address
                          }).ToList();
            emps.Add(new Emp() { No = 21, Name = "suraj", Address = "nasik" });
            // not included in result as due to ToList object created
            //SAME AS  var result = emps.where().select(); -- extension chaining

            Console.WriteLine("Filtered result is ----------");

            foreach (var res in result)
            {
                Console.WriteLine("Name = " + res.EName+ "  lives in  " + res.ECity);

            }

            Console.ReadLine();
            
        }
    }

    public class ResultHolder
    {
        public string EName { get; set; }
        public string ECity { get; set; }
    }
    public class Emp
    {
        public int No { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}
