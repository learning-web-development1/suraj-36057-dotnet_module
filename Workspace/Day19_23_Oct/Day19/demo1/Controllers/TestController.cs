﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using demo1.Models;
namespace demo1.Controllers
{
    public class TestController : BaseController
    {
        // GET: Test
        public ActionResult Index(int? id)
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    Emp emp = new Emp() { No = 1, Name = "rajat", Address = "shimla" };
                    result =  View(emp);

                    break;

                case 2:
                    result = new JsonResult()
                    {
                        Data = dbObj.Emps.ToList(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    break;

                case 3:
                    // give result as js by creating js 
                    result = new JavaScriptResult() { Script = "alert('Hello from JS')"};

                    break;
                case 4:
                    return View("Display");

                    break;

                default:
                    result = new ContentResult() { ContentType = "text/plain", Content = "hello to content type" };
                    break;
            }

           
            


            return result;
        }

        public ActionResult Display()
        {

            return View();
        }

    }
}