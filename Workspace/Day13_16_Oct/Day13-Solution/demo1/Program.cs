﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {


            #region confiuration manager, Sql injection

            /* string sqlconnect = ConfigurationManager.ConnectionStrings["sqlstring"].ToString();

             SqlConnection con = new SqlConnection(sqlconnect);

             try
             {

                 Console.WriteLine("Please Enter your name");

                 #region for select * query
                 *//*    string cmndquery = "select * from Emp where Name = '" + Console.ReadLine() + "' ";

                   SqlCommand cmd = new SqlCommand(cmndquery, con);


                   con.Open();

                   SqlDataReader reader = cmd.ExecuteReader();

                   while (reader.Read())
                   {
                       Console.WriteLine(string.Format("{0} | {1}", reader["Name"], reader["Address"]));
                   }
                    *//*
                 #endregion

                 #region for select count(*) query

                *//* string query = "select count(*) from Emp where Name = '" + Console.ReadLine() + "'";


                 SqlCommand cmd = new SqlCommand(query, con);

                 con.Open();

                 object result = cmd.ExecuteScalar();

                 // int count = (int)result;
                 int count = Convert.ToInt32(result);

                 if (count > 0)
                 {
                     Console.WriteLine("valid user");
                 }
                 else
                 {
                     Console.WriteLine("invalid user");
                 }*//*

                 #endregion

             }
             catch (Exception ex)
             {

                 Console.WriteLine("Error message = " + ex.Message);
             }
             finally
             {
                 con.Close();
             }*/






            #endregion

            #region stored procedure

          


            string sqlstring = ConfigurationManager.
                               ConnectionStrings["sqlstring"].ToString();

            SqlConnection con = new SqlConnection(sqlstring);

            try
            {
                SqlCommand cmd = new SqlCommand("EmpInsert", con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);

                Console.WriteLine("please enter employeee no");
                parameter1.Value = Convert.ToInt32(Console.ReadLine());

                SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);

                Console.WriteLine("please enter employeee name");
                parameter2.Value = Console.ReadLine();

                SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar, 50);

                Console.WriteLine("please enter employeee address");
                parameter3.Value = Console.ReadLine();


                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                cmd.ExecuteNonQuery();

                Console.WriteLine("record inserted using stored procedure");


            }
            catch (Exception ex)
            {

                Console.WriteLine("error message = " + ex.Message);
            }
            finally
            {
                con.Close();
            }


           

            #endregion





            Console.ReadLine();
        }
    }
}
