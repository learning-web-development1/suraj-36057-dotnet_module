﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



    namespace BasicNS
    {
        public class BasicCalculator
        {
            public int Add(int x, int y)
            {
                return x + y;
            }
            public int Sub(int x, int y)
            {
                return x - y;
            }
            public int Mult(int x, int y)
            {
                return x * y;
            }
            public int Div(int x, int y)
            {
                return x / y;
            }


        }

    }

    namespace TempratureNS
    {
        public class TempratureConverter
        {
            public int FarenheitToCelcius(int temp)
            {

            return (temp - 32) * 5 / 9;
        }
        


            public int CelciusToFarenheit(int temp)
            {

            return 9 / 5 * temp + 32;

        }

        }
    }
