﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace library_file
{
   

    public class Employee
    {
        #region data member
        private int _EmpNo;

        private string _Name;

        private string _Designation;

        private double _Salary;




        private double _Commision;

        private int _DeptNo;
        #endregion



        #region getter and setter
        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public double Commision
        {
            get { return _Commision; }
            set { _Commision = value; }
        }

        public double Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }
        #endregion

        #region Data member
        public string Get_Employee_Details()
        {
            return "Employee details : \n " + " " + "No =  " + this.EmpNo.ToString() +
                  " " + "Name =  " + this.Name + " " +
                  "Designation   =  " + this.Designation + " " +
                  "Salary =  " + this.Salary.ToString() + " " +
                  "Commision =  " + this.Commision.ToString() + " " +
                  "DempNo =  " + this.DeptNo.ToString() + " \n";
        }

        #endregion




    }

    public class Department
    {
        private int _DeptNo;

        private string _DeptName;

        private string _Location;



        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }


        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }




        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public string GetDepartmentDetails()
        {
            return "Department details : \n" +
                " dept no : " + this.DeptNo + " " +
                " dept name : " + this.DeptName + " " +
                " dept location : " + this.Location + " \n";
        }


    }


    public class listCreation
    {


        public Dictionary<int, Department> DeptListCreation()
        {
            #region creating DeptList

            Dictionary<int, Department> DeptList = new Dictionary<int, Department>();

            FileStream fs = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment6_11_Oct\dept.csv",
                                            FileMode.OpenOrCreate, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);

            string deptString;

            while ((deptString = reader.ReadLine()) != null)
            {
               // Console.WriteLine(deptString);

                string[] deptDetails = deptString.Split(',');

                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];

                DeptList.Add(dept.DeptNo, dept);
            }
            fs.Flush();
            reader.Close();

            fs.Close();

            return DeptList;
            #endregion

        }

        public Dictionary<int, Employee> EmpListCreation()
        {
            #region Employee List

            Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();



            FileStream fs1 = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment6_11_Oct\emp.csv",
                                             FileMode.OpenOrCreate, FileAccess.Read);

            StreamReader reader2 = new StreamReader(fs1);

            string empString;

            while ((empString = reader2.ReadLine()) != null)
            {
               // Console.WriteLine(empString);

                string[] empDetails = empString.Split(',');

                Employee employee = new Employee();

                employee.EmpNo = int.Parse(empDetails[0]);
                employee.Name = empDetails[1];
                employee.Designation = empDetails[2];
                employee.Salary = double.Parse(empDetails[3]);
                employee.Commision = double.Parse(empDetails[4]);
                employee.DeptNo = int.Parse(empDetails[5]);

                EmpList.Add(employee.EmpNo, employee);


            }



            // empString = null;



            reader2.Close();
            fs1.Close();

            #endregion

            return EmpList;
        }


    }

    public class departmentFunction
    {

        #region get location by foreach or linq 
        public List<string> LocationWithSingleDept(Dictionary<int, Department> DepartmentList)
        {
            List<string> Location = new List<string>();

            #region using linq 
            var deplist = from d in DepartmentList
                          select d.Value.Location;

            foreach (var dplist in deplist)
            {
                Location.Add(dplist);


            }

            bool uniqueList = Location.Distinct().Count() == Location.Count();

            if (uniqueList)
            {
                return Location;
            }
            else
            {
                return null;
            }
            #endregion



            #region using foreach 

            /* foreach (int key in DepartmentList.Keys)
                        {



                            string loc = DepartmentList[key].Location;

                            if (loc != null)
                            {
                                Location.Add(loc);

                            }

                        }
             return Location.Distinct().ToList();
            */

            #endregion

            #region foreach correct approach
            /*
                        foreach (int key in DepartmentList.Keys)
                        {
                            Department d = DepartmentList[key];

                            Location.Add(DepartmentList[key].Location);

                        }

                        bool UniqueList = Location.Distinct().Count() == Location.Count
                            if(UniqueList)
                        {
                            return Location
                        }
                            else
                        {
                            return null;
                        }*/
            #endregion




        }



        

    }

    public class EmployeeFunction
    {

        public List<string> FindDeptsWithNoEmps(Dictionary<int, Employee> EmpList, Dictionary<int, Department> DepartmentList)
        {
            List<string> dept_without_employee = new List<string>();





            #region using foreach

            /* foreach (int key in EmpList.Keys)
             {
                 int count = 0;
                 foreach (int dkey in DepartmentList.Keys)
                 {
                     if(DepartmentList[dkey].DeptNo  == EmpList[key].DeptNo )
                     {
                         count++; 
                     }else
                     {
                         
                     }

                     if(count == 0)
                     {
                         dept_without_employee.Add(DepartmentList[dkey].DeptName);
                     }
                 }
             }
 */
            #endregion


            #region using LINQ
            var empdeptno = from e in EmpList
                            select e.Value.DeptNo;

            foreach (int dkey in DepartmentList.Keys)
            {
                int count = 0;
                foreach (var ekey in empdeptno)
                {
                    if(ekey == DepartmentList[dkey].DeptNo)
                    {
                        count++;
                    }
                }

                if(count == 0)
                {
                    dept_without_employee.Add(DepartmentList[dkey].DeptName);
                }

            }






          /*var noempDept = from e in EmpList
                      join d in DepartmentList on e.Value.DeptNo equals d.Value.DeptNo
                      where  d.Key != e.Value.DeptNo 
                      select d.Value.DeptName.Distinct() ;


            foreach (var dept in noempDept)
            {
                dept_without_employee.Add(Convert.ToString(dept));
            }*/



            /*var deptnoemp = from d in DepartmentList
                      select d.Value.Location;*/

            // var col2 = Orders.Where(o => o.CustomerID == 84);

            // var deptno = DepartmentList.Where(d => d.Value.DeptNo == EmpDeptNo);


            #endregion





            return dept_without_employee;
        }


        public void Calculate_Total_Salary(Dictionary<int, Employee> EmpList, int Key)
        {

            #region by foreach

           /*if (Key == 0)
            {
                foreach (int ekey in EmpList.Keys)
                {
                    double sal = EmpList[ekey].Salary;
                    double comm = EmpList[ekey].Commision;

                    double totalSalary = sal + comm;

                    Console.WriteLine("Employee name = " + " " + EmpList[ekey].Name + " total salary = " + totalSalary);



                }
            }
            else
            {
                double sal = EmpList[Key].Salary;
                double comm = EmpList[Key].Commision;

                double totalSalary = sal + comm;

                Console.WriteLine("Employee name = " + " " + EmpList[Key].Name + " total salary = " + totalSalary);


            }*/

            #endregion

            #region by linq
            var emps = from e in EmpList
                       select new
                       {
                           sal = e.Value.Salary,
                           comm = e.Value.Commision,
                           ename = e.Value.Name,
                           eno = e.Value.EmpNo

                       };

            foreach (var emp in emps)
            {

                Console.WriteLine("employee name " + emp.ename + "  employee salary = " + (emp.sal + emp.comm) + ",");


            }
            #endregion



        }


        public List<Employee> GetAllEmployeesByDept(Dictionary<int, Employee> EmpList, int DeptNo)
        {

            List<Employee> Deptwise = new List<Employee>();

            #region using foreach

            /*foreach (int key in EmpList.Keys)
            {
                if (EmpList[key].DeptNo == DeptNo)

                {
                    Employee emp = new Employee();

                    emp = EmpList[key];
                    Deptwise.Add(emp);
                }
                else
                {

                }

            }
            return Deptwise;*/
            #endregion

            #region using linq 
            var emps = from e in EmpList
                       where e.Value.DeptNo == DeptNo
                       select e.Value;

            foreach (var emp in emps)
            {
                Deptwise.Add(emp);

            }


            return Deptwise;
            #endregion



        }

        public Dictionary<int, double> DeptwiseStaffCount(Dictionary<int, Employee> EmpList)
        {
            Dictionary<int, double> Count = new Dictionary<int, double>();

          
            List<Employee> Deptwise = new List<Employee>();
            listCreation newList1 = new listCreation();

            Dictionary<int, Department> DepartmentList1 = new Dictionary<int, Department>();

            DepartmentList1 = newList1.DeptListCreation();

            #region using foreach
            /*foreach (int dkey in DepartmentList1.Keys)
            {
                double counted = 0.0;
                foreach (int key in EmpList.Keys)
                {
                    if (EmpList[key].DeptNo == DepartmentList1[dkey].DeptNo)

                    {
                        counted++;
                    }

                }
                Count.Add(DepartmentList1[dkey].DeptNo, counted);
            }

            return Count;*/
            #endregion

            #region using linq
            var count = from e in EmpList
                        group e by e.Value.DeptNo into c
                        select new
                        {
                            deptno = c.Key,
                            deptcount = c.Count()
                        };


            foreach (var c in count)
            {
                Count.Add(c.deptno, c.deptcount);
            }

            return Count;
            #endregion

        }


        public Dictionary<int,double> DeptwiseAvgSal(Dictionary<int, Employee> EmpList)
        {
            Dictionary<int, double> Average = new Dictionary<int, double>();


            List<Employee> Deptwise = new List<Employee>();
            listCreation newList1 = new listCreation();

            Dictionary<int, Department> DepartmentList1 = new Dictionary<int, Department>();

            DepartmentList1 = newList1.DeptListCreation();


            #region using foreach
             /*foreach (int dkey in DepartmentList1.Keys)
             {
                 double counted = 0.0;
                 double salary = 0.0;
                 foreach (int key in EmpList.Keys)
                 {
                     if (EmpList[key].DeptNo == DepartmentList1[dkey].DeptNo)

                     {
                         counted++;
                         salary += EmpList[key].Salary;
                     }

                 }
                 double average = salary / counted;

                 Average.Add(DepartmentList1[dkey].DeptNo, average);

                 // Count.Add(DepartmentList1[dkey].DeptNo, counted);
             }

             return Average;*/
            #endregion

            #region using linq 
             var count = from e in EmpList
                        group e.Value.Salary by e.Value.DeptNo into c
                        select new
                        {
                            deptno = c.Key,
                            empcount = c.Count(),
                            deptsalary = c.Sum()
                        };




            foreach (var c in count)
            {
                double avgsal = c.deptsalary / c.empcount;

                Average.Add(c.deptno,avgsal);
            }

            return Average;
            #endregion



        }

        public Dictionary<int, double> DeptwiseMinSal(Dictionary<int, Employee> EmpList)
        {
            Dictionary<int, double> minimumSal = new Dictionary<int, double>();


            List<Employee> Deptwise = new List<Employee>();
            listCreation newList1 = new listCreation();

            Dictionary<int, Department> DepartmentList1 = new Dictionary<int, Department>();

            DepartmentList1 = newList1.DeptListCreation();


            #region using foreach
            /*foreach (int dkey in DepartmentList1.Keys)
            {
                double counted = 0.0;
                double finalsalary = 1000000;
                foreach (int key in EmpList.Keys)
                {
                    if (EmpList[key].DeptNo == DepartmentList1[dkey].DeptNo)

                    {
                        counted++;


                        if (EmpList[key].Salary < finalsalary)
                        {
                            finalsalary = EmpList[key].Salary;
                        }
                    }

                }

                minimumSal.Add(DepartmentList1[dkey].DeptNo, finalsalary);


                // Average.Add(DepartmentList1[dkey].DeptNo, average);

                // Count.Add(DepartmentList1[dkey].DeptNo, counted);
            }

            return minimumSal;*/
            #endregion

            #region using linq 
           var count = from e in EmpList
                        group e.Value.Salary by e.Value.DeptNo into c
                        select new
                        {
                            deptno = c.Key,
                            empsalary = c.Min(),
                        };




            foreach (var c in count)
            {

                minimumSal.Add(c.deptno, c.empsalary);
                
            }

            return minimumSal;
            #endregion
        }

        public Dictionary<int,string> GetEmpInfo(Dictionary<int, Employee> EmpList, Dictionary<int, Department> DepartmentList)
        {
            Dictionary<int, string> Empinfo = new Dictionary<int, string>();

            #region using linq 
            var empinfo = from e in EmpList
                          join d in DepartmentList on e.Value.DeptNo equals d.Value.DeptNo
                          select new
                          {
                              empno = e.Value.EmpNo,
                              empname = e.Value.Name,
                              deptname = d.Value.DeptName
                          };


            foreach (var emp in empinfo)
            {

                Empinfo.Add(emp.empno, "  employee name = " + emp.empname + "  and department name = " + emp.deptname);
            }

            return Empinfo;
            #endregion



        }
    }

}

#endregion