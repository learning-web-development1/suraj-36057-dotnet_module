
Q. Difference between Class and Structure in C#

1. A class is a user-defined blueprint or prototype from which objects are created. 
 -  Basically, a class combines the fields and methods(member function which defines actions) into a single unit.

2. A structure is a collection of variables of different data types under a single unit. 
  -  It is almost similar to a class because both are user-defined data types and both hold a bunch of different data types.

3. Difference between structure and class

      1. Classes are of reference types,
         Structs are of value types.
      2. All the reference types are allocated on heap memory,
        All the value types are allocated on stack memory.
      3. Allocation of large reference type is cheaper than allocation of large value type,
  Allocation and de-allocation is cheaper in value type as compare to reference type.
      4. Class has limitless features,
  Struct has limited features.
      5. Class is generally used in large programs.
  Struct are used in small programs.
      6. Classes can contain constructor or destructor.
  Structure does not contain parameter less constructor or destructor, but can contain Parameterized constructor or static constructor.
      7. Classes used new keyword for creating instances.
  Struct can create an instance, with or without new keyword
      8. A Class can inherit from another class,
  A Struct is not allowed to inherit from another struct or class.
      9. The data member of a class can be protected.
  The data member of struct can’t be protected.
     10. Function member of the class can be virtual or abstract.
  Function member of the struct cannot be virtual or abstract.
     11. Two variable of class can contain the reference of the same object and any operation on one variable can affect another variable.
  Each variable in struct contains its own copy of data(except in ref and out parameter variable) and any operation on one variable can not effect another variable.





|CLASS|STRUCTURE  |
|---------|-------| 
| Classes are of reference types|Structs are of value types.|
| All the reference types are allocated on heap memory.|  All the value types are allocated on stack memory.|
| Allocation of large reference type is cheaper than allocation of large value type.| Allocation and de-allocation is cheaper in value type as compare to reference type. |
|Class has limitless features. | Struct has limited features. |
| Class is generally used in large programs.|Struct are used in small programs  |
|Classes can contain constructor or destructor. | Structure does not contain parameter less constructor or destructor, but can contain Parameterized constructor or static constructor. |
| Classes used new keyword for creating instances.|Struct can create an instance, with or without new keyword  |
|A Class can inherit from another class. | A Struct is not allowed to inherit from another struct or class. |
| The data member of a class can be protected.| The data member of struct can’t be protected. |
|Function member of the class can be virtual or abstract. |  Function member of the struct cannot be virtual or abstract.|
|Two variable of class can contain the reference of the same object and any operation on one variable can affect another variable. |Each variable in struct contains its own copy of data(except in ref and out parameter variable) and any operation on one variable can not effect another variable.   |
| |




