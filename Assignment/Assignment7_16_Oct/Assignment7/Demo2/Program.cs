﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo2.DataAccessLayer;
using Demo2.PlainOldClrObject;
namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            char ischoice = 'y';
            do
            {
                DbFactory dbfactory = new DbFactory();
                IDatabase db = dbfactory.GetDatabase();
                Console.WriteLine("Please enter operation no ,to perform on Emp table : \n 1. Select \n 2. Insert \n 3.Update \n 4. Delete \n ");
                int choice = Convert.ToInt32(Console.ReadLine());


                switch (choice)
                {

                    case 1:

                        List<Emp> emp = new List<Emp>();

                        emp = db.Select();

                        foreach (Emp emps in emp)
                        {
                            
                            
                                Console.WriteLine(string.Format("EMployee No = {0} , Employee Name = {1} , EMployee Address = {2} ",
                               emps.No, emps.Name, emps.Address));
                            
                           
                        }

                        break;
                    case 2:

                       

                        Emp emp1 = new Emp();

                        Console.WriteLine("Please enter employee no");
                        emp1.No = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("please enter employee name");
                        emp1.Name = Console.ReadLine();


                        Console.WriteLine("please enter employee address");
                        emp1.Address = Console.ReadLine();

                        int rowAffected = db.Insert(emp1);

                        Console.WriteLine("No of rows Affected = " + rowAffected);

                        break;
                    case 3:
                        Emp emp2 = new Emp();

                        Console.WriteLine("Please enter employee no to update");
                        emp2.No = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("please enter  new employee name");
                        emp2.Name = Console.ReadLine();


                        Console.WriteLine("please enter new employee address");
                        emp2.Address = Console.ReadLine();

                        int rowAffected2 = db.Update(emp2);


                        Console.WriteLine("No of rows Affected = " + rowAffected2);
                        break;
                    case 4:
                        Emp emp3 = new Emp();

                        Console.WriteLine("Please enter employee no to delete");
                        emp3.No = Convert.ToInt32(Console.ReadLine());



                        int rowAffected3 = db.Delete(emp3);


                        Console.WriteLine("No of rows Affected = " + rowAffected3);
                        break;


                    default:
                        break;
                }

                Console.WriteLine("do you want to continue ? enter y ");

                ischoice = Convert.ToChar(Console.ReadLine());
            } while (ischoice == 'y');


          





        }
    }
}
